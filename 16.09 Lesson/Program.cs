﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _16._09_Lesson
{
    class Program
    {
        static void Main(string[] args)
        {
            Bank bank1 = new Bank("Leumi", "Yehud");
            Customer customer1 = new Customer(666, "John", 123456);
            Account account1 = new Account(customer1, 12000);
            Account account2 = new Account(customer1, 6000);
            
            int addingToBalanceOfCustomer1 = account2 + 1000;

            bank1.AddNewCustomer(customer1);
            bank1.OpenNewAccount(account1, customer1);
            bank1.OpenNewAccount(account2, customer1);

            bank1.Deposit(account1, 10000);
            bank1.Deposit(account2, 5000);
            Account account3 = account1 + account2;
            Console.WriteLine(bank1.GetCustomerTotalBalance(customer1));
            Console.WriteLine(account3.Balance);

            Account account4 = bank1.JoinAccounts(account1, account2);
            Console.WriteLine(account4.Balance);
            bank1.GetAccountsByCustomer(customer1).ForEach(A => Console.WriteLine(A));
            
        }
    }
}
