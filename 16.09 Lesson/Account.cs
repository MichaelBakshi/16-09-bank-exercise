﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _16._09_Lesson
{
    class Account
    {
        private static int numberOfAcc;
        private readonly int accountNumber;
        private readonly Customer accountOwner;
        private int maxMinusAllowed;
        public int AccountNumber {get { return accountNumber; } }
        public int Balance { get; private set; }
        public Customer AccountOwner { get { return accountOwner; } }
        public int MaxMinusAllowed { get { return maxMinusAllowed; } }

        public Account (Customer customer, int monthlyIncome)
        {
            accountOwner = customer;
            maxMinusAllowed = monthlyIncome * 3;
            accountNumber = numberOfAcc++ ;
        }

        public int Add (int amount)
        {
            Balance += amount;
            return Balance;
        }

        public int Subtract(int amount)
        {
            if ((Balance - amount) < maxMinusAllowed)
                throw new BalanceException($"Your request to withdraw {amount} NIS exceeds your maximum minus allowed of {maxMinusAllowed}");
            else
            Balance -= amount;
            return Balance;
        }

        public static bool operator ==(Account account1, Account account2)
        {
            if (account1 is null && account2 is null)
            {
                return true;
            }
            else if (account1 is null || account2 is null)
            {
                return false;
            }
            return account1.accountNumber == account2.accountNumber;
        }

        public static bool operator !=(Account account1, Account account2)
        {
            if (account1 is null && account2 is null)
            {
                return false;
            }
            else if (account1 is null || account2 is null)
            {
                return true;
            }
            return account1.accountNumber != account2.accountNumber;
        }

        public static int operator +(Account account, int amount)
        {
            return account.Balance + amount;
        }

        public static int operator -(Account account, int amount)
        {
            if ((account.Balance - amount) < account.maxMinusAllowed)
                throw new BalanceException($"Your request exceeds your maximum minus allowed of {account.maxMinusAllowed}");
            return account.Balance - amount;
        }

        public static Account operator +(Account account1,Account account2)
        {
            if (account1.accountOwner != account2.accountOwner)
                throw new NotSameCustomerException("Account belong to different owners");
            if (account1.accountNumber == account2.accountNumber)
                throw new AccountAlreadyExistsException("This is the same account number");
            int highest = Math.Max(account1.MaxMinusAllowed, account2.MaxMinusAllowed);
            Account newAccount = new Account(account1.accountOwner, highest/3);
            newAccount.Balance = account1.Balance + account2.Balance;
            return newAccount;
        }

        public bool Equals(Account otherAccount)
        {
            return this.accountNumber == otherAccount.accountNumber;
        }
        public long Hashcode()
        {
            return accountNumber.GetHashCode();
        }

        public override string ToString()
        {
            return $"account number: {AccountNumber}, account owner: {AccountOwner}, balance: {Balance}, maximum minimum allowed: {MaxMinusAllowed}";
        }
    }
}
