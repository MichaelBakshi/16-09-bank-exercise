﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _16._09_Lesson
{
    class Customer
    {
        private static int numberOfCust;
        private readonly int customerID;
        private readonly int customerNumber;

        public string Name { get; private set; }
        public int PhoneNumber { get; private set; }
        public int CustomerID { get { return customerID; } }
        public int CustomerNumber { get { return customerID; } }

        public Customer(int id, string name, int phone)
        {
            this.customerID = id;
            this.Name = name;
            this.PhoneNumber = phone;
            customerNumber++;
        }

        public override int GetHashCode()
        {
            return customerID;
        }

        public static bool operator ==(Customer customer1, Customer customer2)
        {
            if (customer1 is null && customer2 is null)
            {
                return true;
            }
            else if (customer1 is null || customer2 is null)
            {
                return false;
            }
            return customer1.customerNumber == customer2.customerNumber;
        }

        public static bool operator !=(Customer customer1, Customer customer2)
        {
            if (customer1 is null && customer2 is null)
            {
                return false;
            }
            else if (customer1 is null || customer2 is null)
            {
                return true;
            }
            return customer1.customerNumber != customer2.customerNumber;     
        }
        
        public bool Equals (Customer otherCustomer)
        {
            return this.customerNumber == otherCustomer.customerNumber;
        }
        public  long Hashcode ()
        {
            return customerNumber.GetHashCode(); 
        }

    }
}
