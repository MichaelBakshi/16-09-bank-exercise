﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _16._09_Lesson
{
    class Bank:IBank
    {
        private List<Account> _accounts;
        private List<Customer> _customers;
        private Dictionary<int, Customer> CustomerIDToCustomerDict;
        private Dictionary<int, Customer> CustomerNumberToCustomerDict;
        private Dictionary<int, Account> AccountNumberToAccountDict;
        private Dictionary<Customer, List<Account>> CustomerToAccountListDict;
        private long _totalMoneyInBank;
        private double _profits;
        private string _name;
        private string _address;
        private int _customerCount;

        public string Name { get { return _name; } }
        public string Address { get { return _address; } }
        public int CustomerCount { get { return _customers.Count; } }

        internal Customer GetCustomerByID(int customerID)
        {
            CustomerIDToCustomerDict.TryGetValue(customerID, out Customer value);
            return value;
        }

        internal Customer GetCustomerByNumber(int customerNumber)
        {
            CustomerNumberToCustomerDict.TryGetValue(customerNumber, out Customer value);
            return value;
        }
        internal Account GetAccountByNumber(int accountNumber)
        {
            AccountNumberToAccountDict.TryGetValue(accountNumber, out Account value);
            return value;
        }
        internal List<Account> GetAccountsByCustomer(Customer customer)
        {
            CustomerToAccountListDict.TryGetValue(customer, out List<Account> value);
            return value;
        }

        internal void AddNewCustomer(Customer customer)
        {
            if (_customers.Contains(customer))
                throw new CustomerAlreadyExistsException($"Customer {customer} already exists in database of our {this._name } customers");
            
            Customer result = null;
            foreach (var item in _customers)
            {
                if (item.CustomerID==customer.CustomerID)
                {
                    result = customer;
                    break;
                }
            }
            if (result != null)
                throw new CustomerAlreadyExistsException($"Customer {customer} with this ID already esists in our list");

            Customer result2 = null;
            foreach (var item in _customers)
            {
                if (item.CustomerNumber==customer.CustomerNumber)
                {
                    result2 = customer;
                    break;
                }
            }
            if (result2 != null)
                throw new CustomerAlreadyExistsException($"Customer with the same cusomer number already exists in our database");

            if (CustomerIDToCustomerDict.ContainsKey(customer.CustomerID))
                throw new CustomerAlreadyExistsException("Customer with the same ID already exists");

            if (CustomerNumberToCustomerDict.ContainsKey(customer.CustomerNumber))
                throw new CustomerAlreadyExistsException("Customer with the same customer number already exists");

            if (CustomerToAccountListDict.ContainsKey(customer))
                throw new CustomerAlreadyExistsException($"Customer {customer} already exists in Customers' accounts list");

            _customers.Add(customer);
            CustomerIDToCustomerDict.Add(customer.CustomerID, customer);
            CustomerNumberToCustomerDict.Add(customer.CustomerNumber, customer);
            CustomerToAccountListDict.Add(customer, new List<Account> ());
        }

        internal void OpenNewAccount (Account account, Customer customer)
        {
            if (!_customers.Contains(customer))
                throw new CustomerNotFoundException($"There is no such customer in bank {this._name}");
            if (_accounts.Contains(account))
                throw new AccountAlreadyExistsException($"Such account already exists in bank {this._name}");
            if (AccountNumberToAccountDict.ContainsKey(account.AccountNumber))
                throw new AccountAlreadyExistsException($"Such account already exists in bank {this._name}");

            List<Account> listOfAccounts = CustomerToAccountListDict[customer];
            if (listOfAccounts.Contains(account))
                throw new AccountAlreadyExistsException($"Account already exists in customers' accounts list");
            listOfAccounts.Add(account);
            _accounts.Add(account);
            AccountNumberToAccountDict.Add(account.AccountNumber, account);
        }

        internal int Deposit (Account account, int amount)
        {
            if (!_accounts.Contains(account))
                throw new AccountNotFoundException($"Such account doesn't exist in bank {this._name}");

            account.Add(amount);
            _totalMoneyInBank += amount;
            return account.Balance;
        }

        internal int Withdraw (Account account, int amount)
        {
            if (!_accounts.Contains(account))
                throw new AccountNotFoundException($"Such account doesn't exist in bank {this._name}");

            account.Subtract(amount);
            _totalMoneyInBank -= amount;
            return account.Balance;
        }

        internal int GetCustomerTotalBalance(Customer customer)
        {
            if (!CustomerToAccountListDict.ContainsKey(customer))
                throw new CustomerNotFoundException($"Customer doesn't appear in our {this._name} bank occounts list");

            List<Account> accountsOfThisCustomer = CustomerToAccountListDict[customer];
            int totalSum = 0;
            foreach (var item in accountsOfThisCustomer)
            {
                totalSum += item.Balance;
            }
            return totalSum;
        }

        internal void CloseAccount (Account account, Customer customer)
        {
            if (!_accounts.Contains(account))
                throw new AccountNotFoundException($"Account not found");

            if (!AccountNumberToAccountDict.ContainsKey(account.AccountNumber))
                throw new AccountNotFoundException($"Account not found");

            if (!_customers.Contains(customer))
                throw new CustomerNotFoundException($"Customer not found");

            if (account.AccountOwner != customer)
                throw new NotSameCustomerException($"This account doesn't belong to this customer");
            
            AccountNumberToAccountDict.Remove(account.AccountNumber);
            List<Account> accountsOfThisCustomer = CustomerToAccountListDict[customer];
            if (!accountsOfThisCustomer.Contains(account))
                throw new AccountNotFoundException($"Account not found");
            accountsOfThisCustomer.Remove(account);

            _accounts.Remove(account);
        }

        internal void ChargeAnnualCommission(float percentage)
        {
            foreach (var item in _accounts)
            {
                double amla = Math.Abs( item.Balance * percentage);
                item.Subtract((int)amla);
            }
            
        }

        internal Account JoinAccounts(Account account1, Account account2)
        {
            if (!_accounts.Contains(account1))
                throw new AccountNotFoundException($"Account not found");
            if (!_accounts.Contains(account2))
                throw new AccountNotFoundException($"Account not found");
            if (account1.AccountOwner != account2.AccountOwner)
                throw new NotSameCustomerException($"Accounts don't belong to the same customer");
            if (account1 == account2)
                throw new SameAccountException($"This is the same account");

            Customer accountOwner = account1.AccountOwner;
            Account cumulativeAccount = account1 + account2;
            OpenNewAccount(cumulativeAccount, accountOwner);

            CloseAccount(account1, accountOwner);
            CloseAccount(account2, accountOwner);

            return cumulativeAccount;
        }

        public Bank(string name, string address)
        {
            this._name = name;
            this._address = address;

            _accounts = new List<Account>();
            _customers = new List<Customer>();

            CustomerIDToCustomerDict = new Dictionary<int, Customer>();
            CustomerNumberToCustomerDict = new Dictionary<int, Customer>();
            AccountNumberToAccountDict = new Dictionary<int, Account>();
            CustomerToAccountListDict = new Dictionary<Customer, List<Account>>();
        }

        public override string ToString()
        {
            return $"Bank: {_name}, Address: {_address}, Customers: {_customerCount} ";
        }
    }
}
